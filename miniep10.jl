function max_sum_submatrix(matrix)
        max_sum = 0
        max_matrix = []
        for t in 1:size(matrix,1)
                for i in 1:size(matrix,1)-t+1
                        for j in 1:size(matrix,2)-t+1
                                sum = 0
                                for r in i:i+t-1
                                        for s in j:j+t-1
                                                sum = sum + matrix[r,s]
                                        end
                                end
                                if sum > max_sum
                                        max_matrix = []
                                        max_sum = sum
                                        push!(max_matrix,matrix[i:(i+t-1),j:(j+t-1)])
                                elseif sum == max_sum
                                        push!(max_matrix,matrix[i:(i+t-1),j:(j+t-1)])
                                end
                        end
                end
        end
        return max_matrix
end
